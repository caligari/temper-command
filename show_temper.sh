#!/bin/bash

TEMPER='/home/caligari/lib/temper-1.0/temper'

T1=''
T2=''
while [ 1 ] ; do

  T1=$($TEMPER)
  if [ "$T1" != "$T2" ] ; then
    echo "$(date)	$T1"
    T2=$T1
  fi

  sleep 30

done



